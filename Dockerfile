FROM python:3.7.4

RUN mkdir /code
COPY . /code
WORKDIR /code

RUN chmod +x docker-entrypoint.sh
RUN pip install -r requirements.txt
