from sc_server.settings.base import *

SECRET_KEY = 'fg#wsr5j9&3ah0s6=l*oke52g7wk&8_w^o#5x68%c4e_t_eb(7'
DEBUG = False

ALLOWED_HOSTS = [os.environ.get('ALLOWED_HOST_DOMAIN'), os.environ.get('ALLOWED_HOST_IP'), "localhost", "0.0.0.0",
                 "127.0.0.1"]

DATABASES = {
    'default': {
        'CONN_MAX_AGE': 30,
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'HOST': 'db',
        'NAME': 'ksu_db',
        'PASSWORD': 'ksu_passwd',
        'PORT': 5432,
        'USER': 'postgres'
    }
}

STATIC_URL = '/static/'
MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, '../../media')
STATIC_ROOT = os.path.join(BASE_DIR, '../../static')
