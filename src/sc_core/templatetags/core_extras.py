from django import template
from django.utils.translation import ugettext_lazy as _
from phonenumber_field.widgets import PhonePrefixSelect

from sc_auth.forms import LoginForm, SignUpForm

register = template.Library()


@register.filter(name="cut_list")
def cut_list(array, amount):
    return array[:amount]


@register.inclusion_tag('sc_auth/modals/auth_modal.html')
def show_auth_modals():
    sign_up_form = SignUpForm(initial={'phone_number': PhonePrefixSelect(initial="UA").initial,
                                       'signup_country': "UA"})
    login_form = LoginForm()
    return {'sign_up_form': sign_up_form,
            'login_form': login_form}
