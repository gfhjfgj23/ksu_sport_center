from django.apps import AppConfig


class ScCoreConfig(AppConfig):
    name = 'sc_core'
