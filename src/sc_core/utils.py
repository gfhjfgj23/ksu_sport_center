from enum import Enum
import inspect
import string
from django.utils.translation import ugettext_lazy as _
from django import forms
from django.http import HttpResponseRedirect
from django.utils.decorators import method_decorator
from functools import wraps
from django.views.decorators.csrf import csrf_protect
from django.urls import reverse


class ChoiceEnum(Enum):
    @classmethod
    def choices(cls):
        # get all members of the class
        members = inspect.getmembers(cls, lambda m: not (inspect.isroutine(m)))
        # filter down to just properties
        props = [m for m in members if not (m[0][:2] == '__')]
        # format into django choice tuple
        choices = tuple([(str(p[1].value), p[0]) for p in props])
        return choices

    @classmethod
    def choices_dict(cls):
        return dict(cls.choices())


def validate_password(password):
    # Setup Our Lists of Characters and Numbers
    characters = list(string.ascii_letters)
    numbers = [str(i) for i in range(10)]

    # Assume False until Proven Otherwise
    numCheck = False
    charCheck = False

    # Loop until we Match
    for char in password:
        if not charCheck:
            if char in characters:
                charCheck = True
        if not numCheck:
            if char in numbers:
                numCheck = True
        if numCheck and charCheck:
            break

    if not numCheck or not charCheck:
        raise forms.ValidationError(_('Пароль має містити хоча б одну букву і одну цифру.'))


class SubmitGetRedirectMixin(object):
    def get(self, request, *args, **kwargs):
        return HttpResponseRedirect(reverse('pages-root'))


class SubmitCSFRProtectMixin(SubmitGetRedirectMixin):
    @method_decorator(csrf_protect)
    def dispatch(self, request, *args, **kwargs):
        return super(SubmitCSFRProtectMixin, self).dispatch(request, *args, **kwargs)


# for signals
def disable_for_loaddata(signal_handler):
    @wraps(signal_handler)
    def wrapper(*args, **kwargs):
        if kwargs['raw']:
            return
        signal_handler(*args, **kwargs)

    return wrapper
