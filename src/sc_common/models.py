from django.db import models
from cms.plugin_base import CMSPlugin
from filer.fields.image import FilerImageField
from django.utils.translation import ugettext_lazy as _
from phonenumber_field.modelfields import PhoneNumberField
from djangocms_text_ckeditor.fields import HTMLField


class BannerModel(CMSPlugin):
    title = models.CharField(max_length=32, null=False, blank=True, verbose_name=_("Заголовок"))
    description = models.CharField(max_length=256, null=False, blank=True, verbose_name=_("Опис"))
    image = FilerImageField(null=False, blank=True, on_delete=models.CASCADE, related_name="main_banner",
                            verbose_name=_("Зображення"))


class AboutUsModel(CMSPlugin):
    description = HTMLField(max_length=256, null=False, blank=True, verbose_name=_("Опис"))
    image_bottom = FilerImageField(null=False, blank=True, on_delete=models.CASCADE, related_name="about_us_bottom",
                                   verbose_name=_("Зображення знизу"))
    image_left = FilerImageField(null=False, blank=True, on_delete=models.CASCADE, related_name="about_us_left",
                                 verbose_name=_("Зображення зліва"))
    image_right = FilerImageField(null=False, blank=True, on_delete=models.CASCADE, related_name="about_us_right",
                                  verbose_name=_("Зображення зправа"))


class Slogan(models.Model):
    slogan_text = models.CharField(max_length=128, null=False, blank=True, verbose_name=_("Текст слогану"))

    def __str__(self):
        return self.slogan_text

    class Meta:
        verbose_name = _("Слоган")
        verbose_name_plural = _("Слогани")


class ContactsModel(CMSPlugin):
    phone_number = PhoneNumberField(verbose_name=_("Номер телефону"))
    address = models.CharField(max_length=128, null=False, blank=False, verbose_name=_("Адресса"))
    email = models.EmailField(null=False, blank=False)
