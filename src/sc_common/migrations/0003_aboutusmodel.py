# Generated by Django 2.2.6 on 2019-11-04 05:05

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import filer.fields.image


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0022_auto_20180620_1551'),
        migrations.swappable_dependency(settings.FILER_IMAGE_MODEL),
        ('sc_common', '0002_auto_20191104_0659'),
    ]

    operations = [
        migrations.CreateModel(
            name='AboutUsModel',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, related_name='sc_common_aboutusmodel', serialize=False, to='cms.CMSPlugin')),
                ('description', models.CharField(blank=True, max_length=256, verbose_name='Опис')),
                ('image_bottom', filer.fields.image.FilerImageField(blank=True, on_delete=django.db.models.deletion.CASCADE, related_name='about_us_bottom', to=settings.FILER_IMAGE_MODEL, verbose_name='Зображення знизу')),
                ('image_left', filer.fields.image.FilerImageField(blank=True, on_delete=django.db.models.deletion.CASCADE, related_name='about_us_left', to=settings.FILER_IMAGE_MODEL, verbose_name='Зображення зліва')),
                ('image_right', filer.fields.image.FilerImageField(blank=True, on_delete=django.db.models.deletion.CASCADE, related_name='about_us_right', to=settings.FILER_IMAGE_MODEL, verbose_name='Зображення зправа')),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
    ]
