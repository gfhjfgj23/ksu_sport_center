from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from django.utils.translation import ugettext_lazy as _

from sc_common.models import BannerModel, AboutUsModel, Slogan, ContactsModel
from sc_common.services.CommonServices import get_random_slogan


class BannerPlugin(CMSPluginBase):
    module = _("Загальний додаток")
    name = _("Головний банер")
    render_template = "sc_common/plugins/banner.html"
    model = BannerModel


class AboutUsPlugin(CMSPluginBase):
    module = _("Загальний додаток")
    name = _("Про нас")
    render_template = "sc_common/plugins/about_us.html"
    model = AboutUsModel


class SloganPlugin(CMSPluginBase):
    module = _("Загальний додаток")
    name = _("Слоган")
    render_template = "sc_common/plugins/slogan.html"

    def render(self, context, instance, placeholder):
        context["slogan"] = get_random_slogan()
        return context


class ContactsPlugin(CMSPluginBase):
    module = _("Загальний додаток")
    name = _("Контакти")
    render_template = "sc_common/plugins/contacts.html"
    model = ContactsModel


class BreadcrumbsPugin(CMSPluginBase):
    module = _("Загальний додаток")
    name = _("Хлібні крихти")
    render_template = "sc_common/plugins/breadcrumbs.html"


plugin_pool.register_plugin(BannerPlugin)
plugin_pool.register_plugin(AboutUsPlugin)
plugin_pool.register_plugin(SloganPlugin)
plugin_pool.register_plugin(ContactsPlugin)
plugin_pool.register_plugin(BreadcrumbsPugin)
