from sc_common.models import Slogan
from random import randint


def get_random_slogan():
    slogans = Slogan.objects.all()
    if slogans.exists():
        count = slogans.count()
        random_index = randint(0, count - 1)
        return slogans[random_index]
    return None
