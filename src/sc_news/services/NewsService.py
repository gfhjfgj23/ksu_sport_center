from sc_news.models import News


def get_all_news():
    return News.objects.all().order_by("-created_at")


def get_news_by_sport_object(sport_object):
    return get_all_news().filter(sportobject=sport_object)


def get_news_by_sport_club(sport_club):
    return get_all_news().filter(sportclub=sport_club)
