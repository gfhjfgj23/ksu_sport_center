from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class ScNewsConfig(AppConfig):
    name = 'sc_news'
    verbose_name = _("Новини")
