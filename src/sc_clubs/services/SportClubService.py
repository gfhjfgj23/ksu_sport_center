from sc_clubs.models import SportClub, SportObject
from sc_core.common.Exceptions import UnexceptedError, NotFound
from sc_auth.models import UserClubSubscription


def get_all_sport_clubs():
    return SportClub.objects.all()


def get_all_sport_objects():
    return SportObject.objects.all()


def get_sport_clubs_by_sport_object(sport_object):
    try:
        result_set = get_all_sport_clubs().filter(sport_object=sport_object).order_by('id')
        return result_set
    except SportObject.DoesNotExist:
        raise NotFound('This club does not exist')
    except Exception as e:
        raise UnexceptedError(e.__str__())


def get_sport_clubs_by_sport_object_or_all(sport_object=None):
    try:
        if sport_object:
            result_set = get_sport_clubs_by_sport_object(sport_object)
        else:
            result_set = get_all_sport_clubs()
        return result_set
    except Exception as e:
        raise UnexceptedError(e.__str__())


def if_subscribed_to_club(sport_club, user):
    try:
        return UserClubSubscription.objects.filter(sportclub=sport_club, user=user).exists()
    except Exception as e:
        raise UnexceptedError(e.__str__())


def get_sport_club_by_slug(slug):
    try:
        return get_all_sport_clubs().filter(slug=slug).get()
    except SportClub.DoesNotExist:
        raise NotFound('This SportClub does not exist')
    except Exception as e:
        raise UnexceptedError(e.__str__())
