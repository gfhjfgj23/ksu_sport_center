# Generated by Django 2.2.6 on 2019-11-05 10:03

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sc_clubs', '0009_auto_20191105_1203'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='clubscheduleday',
            options={'ordering': ['day_order'], 'verbose_name': 'Спортивний гурток', 'verbose_name_plural': 'Графік роботи'},
        ),
    ]
