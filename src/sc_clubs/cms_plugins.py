from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from django.utils.translation import ugettext_lazy as _

from sc_clubs.services.SportClubService import get_sport_clubs_by_sport_object_or_all, get_all_sport_objects


class SportClubsList(CMSPluginBase):
    module = _("Спортивнi гуртки та об'екти")
    name = _("Спортивнi гуртки")
    render_template = "sc_clubs/plugins/sport_clubs_list_plugin.html"

    def render(self, context, instance, placeholder):
        context["sport_clubs_list"] = get_sport_clubs_by_sport_object_or_all(context.get("sport_object"))
        context['instance'] = instance
        return context

    def get_render_template(self, context, instance, placeholder):
        return self.render_template


class SportObjectsList(CMSPluginBase):
    module = _("Спортивнi гуртки та об'екти")
    name = _("Спортивнi об'єкти")
    render_template = "sc_clubs/plugins/sport_objects_list_plugin.html"

    def get_render_template(self, context, instance, placeholder):
        context["sport_objects_list"] = get_all_sport_objects()
        return self.render_template


class SportObjectDetail(CMSPluginBase):
    module = _("Спортивнi гуртки та об'екти")
    name = _("Спортивний об'єкт - детально")
    render_template = "sc_clubs/plugins/sport_object_detail_plugin.html"


class SportClubDetail(CMSPluginBase):
    module = _("Спортивнi гуртки та об'екти")
    name = _("Спортивний гурток - детально")
    render_template = "sc_clubs/plugins/sport_club_detail_plugin.html"


plugin_pool.register_plugin(SportClubsList)
plugin_pool.register_plugin(SportObjectsList)
plugin_pool.register_plugin(SportClubDetail)
plugin_pool.register_plugin(SportObjectDetail)
