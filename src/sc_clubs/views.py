from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.urls import reverse

from django.shortcuts import HttpResponseRedirect

from sc_clubs.models import SportClub, SportObject
from sc_clubs.services.SportClubService import get_sport_clubs_by_sport_object, get_all_sport_objects, \
    if_subscribed_to_club, get_sport_club_by_slug
from sc_core.utils import SubmitCSFRProtectMixin
from django.views.generic.edit import BaseFormView
from sc_clubs.forms import SubscribeForm
from sc_auth.models import UserClubSubscription

from django.contrib import messages


class SportObjectList(ListView):
    model = SportObject
    context_object_name = "sport_objects_list"
    template_name = "sc_clubs/sport_objects_list.html"

    def get_context_data(self, **kwargs):
        context = {"sport_objects_list": get_all_sport_objects()}
        context.update(kwargs)
        return super().get_context_data(**context)


class SportObjectDetail(DetailView):
    model = SportObject
    context_object_name = "sport_object"
    template_name = "sc_clubs/sport_object_details.html"

    def get_context_data(self, **kwargs):
        context = {"sport_clubs_list": get_sport_clubs_by_sport_object(self.object)}
        context["meta_title"] = self.object.name
        context.update(kwargs)
        return super().get_context_data(**context)


class SportClubDetail(DetailView):
    model = SportClub
    context_object_name = "sport_club"
    template_name = "sc_clubs/sport_club_details.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        sport_club = context.get("sport_club")
        context["sport_object"] = sport_club.sport_object
        context["is_subscribed"] = False
        context["meta_title"] = self.object.name
        context["schedule"] = sport_club.schedule
        if self.request.user.is_authenticated:
            context["is_subscribed"] = if_subscribed_to_club(sport_club=sport_club, user=self.request.user)
        return context


class SubscriptionView(SubmitCSFRProtectMixin, BaseFormView):
    form_class = SubscribeForm

    def form_valid(self, form):
        comment = form.cleaned_data.get('comment', "")
        slug = self.kwargs.get("slug")
        sport_club = get_sport_club_by_slug(slug)
        UserClubSubscription.objects.create(comment=comment,
                                            user=self.request.user,
                                            sportclub=sport_club)
        return HttpResponseRedirect(self.request.META.get('HTTP_REFERER'))

    def post(self, request, *args, **kwargs):
        self.slug = kwargs.get("slug")
        if request.user.is_authenticated:
            return super(SubscriptionView, self).post(request, *args, **kwargs)
        else:
            messages.error(request, "Ви повинні авторизуватися")
            return HttpResponseRedirect(self.request.META.get('HTTP_REFERER'))

    def render_to_response(self, context):
        form = context.get("form")
        for error in form.errors:
            messages.error(self.request, error)
        return HttpResponseRedirect(self.request.META.get('HTTP_REFERER'))

    def get(self, request, *args, **kwargs):
        return HttpResponseRedirect(reverse('pages-root'))
