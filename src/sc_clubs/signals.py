from sc_clubs.models import ClubScheduleDay
from sc_core.utils import disable_for_loaddata


@disable_for_loaddata
def create_schedule_days_for_club(sender, instance, created, *args, **kwargs):
    if created:
        for i, day in enumerate(["Понеділок", "Вівторок", "Середа", "Четверг", "П'ятниця", "Суббота", "Неділя"]):
            ClubScheduleDay.objects.create(day_name=day, sport_club=instance, day_order=i)
