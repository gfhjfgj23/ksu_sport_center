from django.contrib import admin

# from modeltranslation.admin import TranslationAdmin, TranslationTabularInline

from sc_clubs.models import SportObject, SportClub, ClubScheduleDay
from django_better_admin_arrayfield.admin.mixins import DynamicArrayMixin


class ClubScheduleDayInline(admin.TabularInline):
    model = ClubScheduleDay
    show_change_link = False
    fields = ('day_name', 'datetime_from', 'datetime_to','is_able')
    extra = 0
    can_delete = False

    def has_add_permission(self, request, obj=None):
        return False


class SportClubAdmin(admin.ModelAdmin, DynamicArrayMixin):
    list_display = ('name', 'description')
    prepopulated_fields = {'slug': ('name',)}
    inlines = [ClubScheduleDayInline]
    fieldsets = (
        ('Club info', {
            'fields': (('name', 'sport_object'), 'short_description', 'description')
        }),
        ('Additional', {
            'fields': ('slug',)
        }),
        ('Media', {
            'fields': ('image',)
        }),
    )


class SportObjectClubsInline(admin.TabularInline):
    model = SportClub
    show_change_link = True
    fields = ('name', 'description')
    extra = 0


class SportObjectAdmin(admin.ModelAdmin, DynamicArrayMixin):
    list_display = ('name', 'description')
    prepopulated_fields = {'slug': ('name',)}
    inlines = [SportObjectClubsInline]
    fieldsets = (
        ('Object info', {
            'fields': ('name', 'short_description', 'description')
        }),
        ('Additional', {
            'fields': ('slug',)
        }),
        ('Media', {
            'fields': ('image',)
        }),
    )


admin.site.register(SportObject, SportObjectAdmin)
admin.site.register(SportClub, SportClubAdmin)
