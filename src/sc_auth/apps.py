from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class ScAuthConfig(AppConfig):
    name = 'sc_auth'
    verbose_name = _("Користувачi")
