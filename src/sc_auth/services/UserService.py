from sc_auth.models import CustomUser


def is_user_exist(email=None, username=None):
    if email is not None and CustomUser.objects.filter(email=email).exists():
        result = "Користувач з цією поштою вже існує"
        return True, result
    elif username is not None and CustomUser.objects.filter(username=username).exists():
        result = "Користувач з цим нікнейном вже існує"
        return True, result
    else:
        return False, None
