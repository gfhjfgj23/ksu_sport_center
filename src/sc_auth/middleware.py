from django.utils.deprecation import MiddlewareMixin
from sc_auth.models import UserClubSubscription
from django.contrib import messages


class ClubSubscriptorsCount(MiddlewareMixin):
    def process_response(self, request, response):
        if response.status_code==200 and request.user.is_superuser:
            if "/media/filer" not in request.path and "/admin/" not in request.path:
                count = UserClubSubscription.objects.filter(is_processed=False).count()
                if count:
                    messages.info(request, "%s підписників на секції не оброблено" % count)
        return response
