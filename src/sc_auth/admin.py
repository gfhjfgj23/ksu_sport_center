from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import ugettext_lazy as _
from sc_auth.models import CustomUser, UserClubSubscription


class CustomUserAdmin(UserAdmin):
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_('Personal info'), {'fields': (('first_name', 'last_name'), ('email', 'phone_number'))}),
        (_('Permissions'), {
            'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions'),
        }),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )


class UserClubSubscriptionAdmin(admin.ModelAdmin):
    list_display = ('user', 'sportclub', "created_at", "is_processed")
    readonly_fields = ('created_at', 'user', "sportclub")
    fieldsets = (
        ('', {
            'fields': ('sportclub', 'user')
        }),
        ('', {
            'fields': ('created_at', 'is_processed')
        }),
    )

    def has_add_permission(self, request, obj=None):
        return False


admin.site.register(UserClubSubscription, UserClubSubscriptionAdmin)

admin.site.register(CustomUser, CustomUserAdmin)
