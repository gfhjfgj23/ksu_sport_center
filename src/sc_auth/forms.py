from django import forms

from django.core.validators import validate_email
from django.contrib.auth.validators import ASCIIUsernameValidator
from django.utils.translation import ugettext as _
from phonenumber_field.widgets import PhoneNumberPrefixWidget

from sc_core.utils import validate_password
from phonenumber_field.validators import validate_international_phonenumber


class LoginForm(forms.Form):
    login_username = forms.CharField(max_length=32, required=True,
                                     widget=forms.TextInput(attrs={'placeholder': _('Нікнейм')}), label=False)
    login_password = forms.CharField(max_length=32, required=True,
                                     widget=forms.PasswordInput(attrs={'placeholder': _('Пароль')}), label=False)

    def clean(self):
        form_data = self.cleaned_data
        login_username = self.cleaned_data.get("login_username")
        if login_username:
            ASCIIUsernameValidator()(login_username)
        if not self.cleaned_data.get('login_username', None):
            self._errors["login_username"] = self.error_class(self._errors["login_username"])
        return form_data


class SignUpForm(forms.Form):
    signup_username = forms.CharField(max_length=32, required=True,
                                      widget=forms.TextInput(
                                          attrs={'placeholder': _('Нікнейм'), 'pattern': '.{5,}',
                                                 'title': 'Нікнейм має містити мінімум 5 символів'}),
                                      label=False)
    signup_email = forms.CharField(max_length=32, required=True,
                                   widget=forms.EmailInput(attrs={'placeholder': _('E-mail'),
                                                                  'pattern': '^[\w]{1,}[\w.+-]{0,}@[\w-]{2,}([.][a-zA-Z]{2,}|[.][\w-]{2,}[.][a-zA-Z]{2,})$'}),
                                   label=False)
    signup_first_name = forms.CharField(max_length=30, required=True,
                                        widget=forms.TextInput(attrs={'placeholder': _("Ім'я"),
                                                                      'pattern': "([A-Za-z-'\s]+){2,30}"}),
                                        label=False)
    signup_last_name = forms.CharField(max_length=30, required=True,
                                       widget=forms.TextInput(attrs={'placeholder': _('Прізвище'),
                                                                     'pattern': "([A-Za-z-'\s]+){2,30}"}),
                                       label=False)
    phone_number = forms.CharField(max_length=16, required=True,
                                   widget=PhoneNumberPrefixWidget(attrs={'placeholder': _('Номер телефону'),
                                                                         'class': 'resizeselect',
                                                                         'pattern': "^([0-9]){7,13}$"}),
                                   label=False)
    signup_password = forms.CharField(min_length=6, max_length=32, required=True,
                                      widget=forms.PasswordInput(attrs={'placeholder': _('Пароль'),
                                                                        'pattern': '^(?=.*[A-Za-z$@$!%*#?&])(?=.*\d)[A-Za-z\d$@$!%*#?&]{6,31}$',
                                                                        'title': _(
                                                                            'Пароль має містити 6 символів та хоча б одну букву та цифру'),
                                                                        'data-expect-similar': 'password'}),
                                      label=False)
    signup_confirm_password = forms.CharField(max_length=32, required=True,
                                              widget=forms.PasswordInput(
                                                  attrs={'placeholder': _('Підтвердіть пароль'),
                                                         'title': _("Паролі мають бути однаковими"),
                                                         'data-check-similarity': 'true',
                                                         'data-expect-similar': 'password'}),
                                              label=False)

    def clean(self):
        form_data = self.cleaned_data
        username = self.cleaned_data.get("signup_username")
        email = self.cleaned_data.get("signup_email")
        password = self.cleaned_data.get("signup_password")
        confirm_password = self.cleaned_data.get("signup_confirm_password")
        phone_number = self.cleaned_data.get("phone_number")
        if password:
            validate_password(password)
        if username:
            ASCIIUsernameValidator()(username)
        if email:
            validate_email(email)
        if phone_number:
            validate_international_phonenumber(phone_number)
        if password != confirm_password:
            raise forms.ValidationError(_("Паролі не співпадають"))
        if not self.cleaned_data.get('signup_username', None):
            self._errors["signup_username"] = self.error_class(self._errors["signup_username"])
        return form_data
